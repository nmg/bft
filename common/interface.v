module interface (
	input clk, 
	input reset, 
	input [p_sz-1:0] bus_i,
	output reg [p_sz-1:0] bus_o, 
	input [p_sz-1:0] pe_interface,
	output reg [p_sz-1:0] interface_pe,
	output reg resend
	);

	parameter num_leaves= 2;
	parameter payload_sz= 1;
	parameter addr= 1'b0;
	parameter p_sz= 1 + $clog2(num_leaves) + payload_sz; //packet size

	wire accept_packet;
	wire send_packet;
	assign accept_packet= bus_i[p_sz-1] && 
			(bus_i[p_sz-2:payload_sz] == addr);
	assign send_packet= !(bus_i[p_sz-1] && 
		addr != bus_i[p_sz-2:payload_sz]);


	always @(posedge clk) begin
	//	cache_o <= pe_interface;
		if (reset)
			{resend, interface_pe, bus_o} <= 0;
		else begin
			if (accept_packet)
				interface_pe <= bus_i;
			else
				interface_pe <= 0;
			if (send_packet) begin
			//	if (resend)
	//				bus_o <= cache_o;
	//			else
					bus_o <=  pe_interface;
				resend <= 1'b0;
			end
			else begin
				bus_o <= bus_i;
				resend <= 1'b1;
			end
		end
	end
	
endmodule
