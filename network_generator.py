#!/usr/bin/python3
from math import ceil, log

def clog2(num):
    return int(ceil(log(num, 2)))

def create_subtrees(num_leaves, pattern, num_sent_per_leaf= 10, 
                     level= 0):
    if pattern == '':
        return ''
    switch_type= 't' if pattern[-1] == '1' else 'pi'
    num_switches_in_cluster= 1
    for c in [int(c) for c in pattern[0:-1]]:
        num_switches_in_cluster *= c
    num_leaves_in_subtree= num_leaves // (2**level)
    subtree_name= '_' + pattern[:-1] + 'subtree' if int(pattern) > 2 else 'interface'
    if level > 0:
        addr_str=  str(level) + '\'b'+ level*'0';
        extra_io= '\n'.join([
            ',',
            '\tinput [' + pattern[-1] + '*' + str(num_switches_in_cluster) + 
            '*p_sz-1:0] bus_i,\n',
            '\toutput [' + pattern[-1] + '*' + str(num_switches_in_cluster) + '*p_sz-1:0] bus_o'])
        module_name= '_' + pattern + 'subtree'
    else:
        addr_str= '1\'b0' 
        extra_io= ''
        module_name= 'gen_nw'

    return '\n'.join([
    'module ' + module_name + ' (',
    '\tinput clk,',
    '\tinput reset,',
    '\tinput [p_sz*' + str(num_leaves_in_subtree) + '-1:0] pe_interface,',
    '\toutput [p_sz*' + str(num_leaves_in_subtree) + '-1:0] interface_pe,',
    '\toutput [' + str(num_leaves_in_subtree) + '-1:0] resend' + extra_io,
    ');',
    '\tparameter num_leaves= ' + str(num_leaves) + ';',
    '\tparameter payload_sz= $clog2(num_leaves) + ' + 
        str(clog2(num_sent_per_leaf)) + ';',
    '\tparameter p_sz= 1 + $clog2(num_leaves) + payload_sz; //packet size',
    '\tparameter addr= ' + addr_str + ';',
    '\tparameter level= ' + str(level) + ';',
    '\t',
    '\twire [' + str(num_switches_in_cluster) + '*p_sz-1:0] switch_left;',
    '\twire [' + str(num_switches_in_cluster) + '*p_sz-1:0] switch_right;',
    '\twire [' + str(num_switches_in_cluster) + '*p_sz-1:0] left_switch;',
    '\twire [' + str(num_switches_in_cluster) + '*p_sz-1:0] right_switch;',
    '',
    '\t' + switch_type + '_cluster #(',
    '\t\t.num_leaves(num_leaves),',
    '\t\t.payload_sz(payload_sz),',
    '\t\t.addr(addr),',
    '\t\t.level(level),',
    '\t\t.p_sz(p_sz),',
    '\t\t.num_switches(' + str(num_switches_in_cluster) + '))',
    '\t\t' + switch_type + '_lvl' + str(level) + '(',
    '\t\t\t.clk(clk),',
    '\t\t\t.reset(reset),',
    '\t\t\t.u_bus_i(bus_i),' if level > 0 else '',
    '\t\t\t.u_bus_o(bus_o),' if level > 0 else '',
    '\t\t\t.l_bus_i(left_switch),',
    '\t\t\t.r_bus_i(right_switch),',
    '\t\t\t.l_bus_o(switch_left),',
    '\t\t\t.r_bus_o(switch_right));',
    '',
    '\t' + subtree_name + ' #(',
    '\t\t.num_leaves(num_leaves),',
    '\t\t.payload_sz(payload_sz),',
    '\t\t.addr({' + ('addr, ' if level > 0 else '') +  '1\'b0}),',
    '\t\t.p_sz(p_sz))',
    '\t\tsubtree_left(',
    '\t\t\t.clk(clk),',
    '\t\t\t.reset(reset),',
    '\t\t\t.bus_i(switch_left),',
    '\t\t\t.bus_o(left_switch),',
    '\t\t\t.pe_interface(pe_interface[p_sz*' + 
        str(num_leaves_in_subtree // 2) + '-1:0]),',
    '\t\t\t.interface_pe(interface_pe[p_sz*' + 
        str(num_leaves_in_subtree // 2) + '-1:0]),',
    '\t\t\t.resend(resend[' + str(num_leaves_in_subtree //2 ) + '-1:0]));',
    '',
    '\t' + subtree_name+ ' #(',
    '\t\t.num_leaves(num_leaves),',
    '\t\t.payload_sz(payload_sz),',
    '\t\t.addr({' + ('addr, ' if level > 0 else '') +  '1\'b1}),',
    '\t\t.p_sz(p_sz))',
    '\t\tsubtree_right(',
    '\t\t\t.clk(clk),',
    '\t\t\t.reset(reset),',
    '\t\t\t.bus_i(switch_right),',
    '\t\t\t.bus_o(right_switch),',
    '\t\t\t.pe_interface(pe_interface[p_sz*' + str(num_leaves_in_subtree) + 
        '-1:p_sz*' + str(num_leaves_in_subtree // 2) + ']),',
    '\t\t\t.interface_pe(interface_pe[p_sz*' + str(num_leaves_in_subtree) + 
        '-1:p_sz*' + str(num_leaves_in_subtree // 2) + ']),',
    '\t\t\t.resend(resend[' + str(num_leaves_in_subtree) + '-1:' + 
        str(num_leaves_in_subtree // 2) + ']));',
    'endmodule',
    create_subtrees(num_leaves, pattern[0:-1], level= level + 1)])

def eval_pattern(pattern):
    pattern_list= [int(c) for c in list(pattern)]
    return (sum(list(pattern_list)) / len(pattern_list)) - 1

def create_pattern(num_leaves, p):
    pattern_sz= clog2(num_leaves)
    pattern= ''
    for _ in range(0, pattern_sz):
        if eval_pattern(pattern + '1') < p:
            pattern += '2'
        else:
            pattern += '1'
    return pattern
    
def create_leafless_network(num_leaves, p, num_sent_per_leaf):
    pattern= create_pattern(num_leaves, p)
    return create_subtrees(num_leaves, pattern, num_sent_per_leaf)

