`timescale 1ns / 1ps
module test;

	initial begin
		$dumpfile("t_arbiter.vcd");
		$dumpvars(0,t_arbiter_test);
		#1385 $finish;
	end

	reg [1:0] d_l;
	reg [1:0] d_r;
	reg [1:0] d_u;
	wire [1:0] sel_l;
	wire [1:0] sel_r;
	wire [1:0]sel_u;
	parameter this_level= 1;
	t_arbiter #(.this_level(this_level))
	t_arbiter_test(d_l, d_r, d_u, sel_l, sel_r, sel_u);

	integer i = 0;
	initial begin
		d_l= 0;
		d_r= 0;
		d_u= 0;
		#100;
		
		for ( i = 0; i < 2^6; i = i+1)
		begin
			{d_l, d_r, d_u} <= i; 
			#20;
		end

	end

	reg [48:0] string_d_l, string_d_r, string_d_u, string_sel_l, string_sel_r, 
				string_sel_u;
	always @*
	begin
		case (d_l)
			2'b00: string_d_l= "VOID";                                          
			2'b01: string_d_l= "LEFT";
			2'b10: string_d_l= "RIGHT"; 
			2'b11: string_d_l= "UP";
		endcase   

		case (d_r)
			2'b00: string_d_r= "VOID";                                          
			2'b01: string_d_r= "LEFT";
			2'b10: string_d_r= "RIGHT"; 
			2'b11: string_d_r= "UP";
		endcase   

		case (d_u)
			2'b00: string_d_u= "VOID";                                          
			2'b01: string_d_u= "LEFT";
			2'b10: string_d_u= "RIGHT"; 
			2'b11: string_d_u= "UP";
		endcase   

		case (sel_l)
			2'b00: string_sel_l= "VOID";                                          
			2'b01: string_sel_l= "LEFT";
			2'b10: string_sel_l= "RIGHT"; 
			2'b11: string_sel_l= "UP";
		endcase   

		case (sel_r)
			2'b00: string_sel_r= "VOID";                                          
			2'b01: string_sel_r= "LEFT";
			2'b10: string_sel_r= "RIGHT"; 
			2'b11: string_sel_r= "UP";
		endcase   

		case (sel_u)
			2'b00: string_sel_u= "VOID";                                          
			2'b01: string_sel_u= "LEFT";
			2'b10: string_sel_u= "RIGHT"; 
			2'b11: string_sel_u= "UP";
		endcase   
	end


	initial begin
		#100
		$monitor("d_l:%s\t| d_r:%s\t| d_u: %s\t| sel_l: %s\t| sel_r: %s\t sel_u: %s",
              string_d_l, string_d_r, string_d_u, string_sel_l, string_sel_r,
			 string_sel_u);
	end

      
endmodule
