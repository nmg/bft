#!/usr/bin/python3
from network_generator import clog2

def create_packet_creator(injection_rate, traffic_params, num_sent_per_leaf):
    import random
    from datetime import datetime
    random.seed(datetime.now())

    traffic_pattern= traffic_params['pattern']
    if traffic_pattern == 'complement':
        pattern_str= '~addr'
    elif traffic_pattern == 'random':
        pattern_str= '$random('+ str(random.randint(0, 999)) + ')[$clog2(num_leaves)-1:0]'
    elif traffic_pattern == 'uniform':
        pattern_str= 'addr + ' + str(traffic_params['offset'])
    else:
        print('packet creator error!')
        return ''

    return '\n'.join([
        'module packet_creator (',
        '\tinput clk, ',
        '\tinput reset,',
        '\tinput resend,',
        '\toutput reg [p_sz-1:0] bus_o',
        '\t);',
        '',
        '\tparameter num_leaves= 2;',
        '\tparameter payload_sz = 1; // contains source address',
        '\tparameter addr= 1\'b0;',
        '\tparameter p_sz= 1 + $clog2(num_leaves) + payload_sz; //packet size',
        '\tparameter num_sent_per_leaf= ' + str(num_sent_per_leaf) + ';',
        '\t',
        '\tparameter send_order_sz= $clog2(num_sent_per_leaf);',
        '\treg [send_order_sz:0] i;',
        '\treg send_out;',
        '\tinteger seed= ' + str(random.randint(0,2**32-1)) + ';',
        '\tinitial {i , send_out} <= 0;',
        '\talways @(posedge clk) begin',
        '\t\tif (reset)',
        '\t\t\t{i, send_out, bus_o} <= 0;',
        '\t\telse',
        '\t\t\tif (resend && send_out)',
        '\t\t\t\t\tbus_o <= {1\'b1, ' + pattern_str + ', addr, i[send_order_sz-1:0]};',
        '\t\t\telse begin',
        '\t\t\t\tsend_out <= ($random(seed) % ' + str(100 // injection_rate) + 
            ') == 1\'b0;',
        '\t\t\t\tif (send_out && i < num_sent_per_leaf) begin',
        '\t\t\t\t\t\tbus_o <= {1\'b1, ' + pattern_str + ', addr, i[send_order_sz-1:0]};',
        '\t\t\t\t\t\ti<= i + 1;',
        '\t\t\t\tend',
        '\t\t\t\telse',
        '\t\t\t\t\tbus_o <= 0;',
        '\t\t\tend',
        '\tend',
        'endmodule'])

def create_tb(num_leaves, injection_rate, num_sent_per_leaf):
    send_order_bits= clog2(num_sent_per_leaf)
    return '\n'.join([
    'module test;',
    '',
    '\tparameter num_leaves= ' + str(num_leaves) + ';',
    '\tparameter payload_sz= $clog2(num_leaves) + ' + str(send_order_bits) + ';',
    '\tparameter p_sz= 1 + $clog2(num_leaves) + payload_sz;',
    '',
    '\tinteger k;',
    '',
    '\tinitial begin',
    '\t\t$dumpfile("gen_nw.vcd");',
    '\t\t$dumpvars(0, gen_nw_test);',
    '\t\t$dumpvars(0, test.resend);',
    '\t\tfor (k= 0; k < num_leaves; k= k + 1) begin',
    '\t\t\t$dumpvars(0, test.pe_interface_arr[k]);',
    '\t\t\t$dumpvars(0, test.interface_pe_arr[k]);',
    '\t\t\tend',
    '\t\t#' + str(50 + round(20*num_sent_per_leaf*90*100/injection_rate)), 
    '\t\t$finish;',
    '\tend',
    '',
    '\treg clk;',
    '\treg reset;',
    '\t',
    '\twire [num_leaves*p_sz-1:0] pe_interface;',
    '\twire [num_leaves*p_sz-1:0] interface_pe;',
    '\twire [p_sz-1:0] pe_interface_arr [num_leaves-1:0];',
    '\twire [p_sz-1:0] interface_pe_arr [num_leaves-1:0];',
    '\twire [num_leaves-1:0] resend;',
    '',
    'integer i;',
    '\tinitial begin',
    '\t\t{clk, reset}= 2\'b01;',
    '\t\t#50',
    '\t\tfor (i= 0; i < 2^(100); i= i + 1) begin',
    '\t\t\tif (i == 1) reset <= 0;',
    '\t\t\tclk<= ~clk;',
    '\t\t\t#10;',
    '\t\tend',
    '\tend',
    '',
    '\tgen_nw #(',
    '\t\t.num_leaves(num_leaves),',
    '\t\t.payload_sz(payload_sz),',
    '\t\t.p_sz(p_sz),',
    '\t\t.addr(1\'b0),',
    '\t\t.level(0))',
    '\t\tgen_nw_test(',
    '\t\t\t.clk(clk),',
    '\t\t\t.reset(reset),',
    '\t\t\t.pe_interface(pe_interface),',
    '\t\t\t.interface_pe(interface_pe),',
    '\t\t\t.resend(resend));',
    '',
    '\tgenvar j;',
    '\tgenerate',
    '\tfor (j= 0; j < num_leaves; j=j+1) begin : gen_pe',
    '\t\tassign pe_interface[j*p_sz+:p_sz]= pe_interface_arr[j];',
    '\t\tassign interface_pe_arr[j]=  interface_pe[j*p_sz+:p_sz];',
    '',
    '\t\tpacket_creator #(',
    '\t\t\t.num_leaves(num_leaves),',
    '\t\t\t.payload_sz(payload_sz),',
    '\t\t\t.addr(j[$clog2(num_leaves)-1:0]),',
    '\t\t\t.p_sz(p_sz))',
    '\t\t\tpc(',
    '\t\t\t\t.clk(clk),',
    '\t\t\t\t.reset(reset),',
    '\t\t\t\t.bus_o(pe_interface_arr[j]),', 
    '\t\t\t\t.resend(resend[j]));',
    '\tend',
    '\tendgenerate',
    '',
    '\tinitial $display("type\\tPE\\tpacket\\t\\ttime");',
    '\tgenvar m;',
    '\tgenerate',
    '\tfor(m= 0; m < num_leaves; m= m + 1) begin',
    '\t\talways @(posedge clk) begin',
    '\t\t\tif (pe_interface_arr[m][p_sz-1])',
    '\t\t\t\t$display("sent\\t%05d\\t%b\\t%0d", m, pe_interface_arr[m], $time);',
    '\t\t\tif (interface_pe_arr[m][p_sz-1])',
    '\t\t\t\t$display("rcvd\\t%05d\\t%b\\t%0d", m, interface_pe_arr[m], $time);',
    '\t\tend',
    '\tend',
    '\tendgenerate',
    'endmodule '])



